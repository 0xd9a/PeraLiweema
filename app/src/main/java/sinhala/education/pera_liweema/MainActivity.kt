/*
 *     This file is part of පෙර ලිවීම.
 *
 *     පෙර ලිවීම is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     පෙර ලිවීම is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with පෙර ලිවීම.  If not, see <https://www.gnu.org/licenses/>.
 */

package sinhala.education.pera_liweema

import android.app.Dialog
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.view.doOnPreDraw
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kasun.svg.path_animator.SvgPathAnimatorView


class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setSVG(Glyphs.Glyph1)

        val recyclerView = findViewById<RecyclerView>(R.id.list)
        recyclerView.doOnPreDraw {
            it.layoutParams.height = it.measuredWidth / 5
            it.requestLayout()
        }

        with(recyclerView) {
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
            adapter = RecyclerViewAdapter(Glyphs.values()) { index ->
                setSVG(Glyphs.values()[index])
            }
        }

        val svgPlayer = findViewById<SvgPathAnimatorView>(R.id.svg_player)
        findViewById<View>(R.id.touch_area).setOnClickListener {
            if (svgPlayer.state == SvgPathAnimatorView.STATE_NOT_STARTED || svgPlayer.state == SvgPathAnimatorView.STATE_FINISHED) {
                svgPlayer.start()
            } else {
                svgPlayer.reset()
                Handler(Looper.getMainLooper()).postDelayed(
                    { if (svgPlayer.state == SvgPathAnimatorView.STATE_NOT_STARTED) svgPlayer.start() },
                    250L
                )
            }
        }

        val aboutButton = findViewById<ImageView>(R.id.about_button)

        aboutButton.doOnPreDraw {
            println(aboutButton.measuredHeight)
            it.layoutParams.width = it.measuredHeight
            it.requestLayout()
        }

        aboutButton.setOnClickListener {
            val dialog = Dialog(this)
            dialog.setContentView(R.layout.about_dialog)
            dialog.setCanceledOnTouchOutside(true)
            dialog.window?.setLayout(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
            )
            dialog.findViewById<TextView>(R.id.source_link).setOnClickListener {
                val intent = Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.source_repo)))
                startActivity(intent)
            }
            dialog.findViewById<TextView>(R.id.license_link).setOnClickListener {
                val intent = Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.license_link)))
                startActivity(intent)
            }
            dialog.findViewById<TextView>(R.id.privacy_link).setOnClickListener {
                val intent = Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.privacy_link)))
                startActivity(intent)
            }
            dialog.window?.setDimAmount(0F)
            dialog.show()
        }
    }

    private fun setSVG(glyph: Glyphs) {
        val svgPlayer = findViewById<SvgPathAnimatorView>(R.id.svg_player)
        val svgPreview = findViewById<SvgPathAnimatorView>(R.id.svg_preview)

        svgPreview.reset()
        svgPreview.setGlyphStrings(glyph.glyph)
        svgPreview.setViewportSize(256F, 256F)
        svgPreview.strokeWidth = 5F
        svgPreview.strokeColor = ContextCompat.getColor(this, R.color.hint)
        svgPreview.rebuildGlyphData()
        svgPreview.setToFinishedFrame()

        svgPlayer.reset()
        svgPlayer.setGlyphStrings(glyph.glyph)
        svgPlayer.setViewportSize(256F, 256F)
        svgPlayer.strokeWidth = 5F
        svgPlayer.strokeColor = ContextCompat.getColor(this, R.color.foreground)
        svgPlayer.speedMultiplier = 1F
        svgPlayer.delayBetweenPaths = 150
        svgPlayer.rebuildGlyphData()
    }
}