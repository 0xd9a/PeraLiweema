### Privacy Policy For 'Pera Liweema'

This app does not collect, share or send any personal information.

<br>

Children's Information

I do not knowingly collect any personal information from children under the age of 13. If you think that your child provided this kind of information to me, I strongly encourage you to contact me immediately and I will do my best efforts to promptly remove such information from my records.